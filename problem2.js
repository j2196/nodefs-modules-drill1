const fs = require('fs');
const path = require('path');

const performMultipleActions = () => {
    fs.readFile(path.join(__dirname, "lipsum.txt"), "utf8", (error, data) => {
        if (error) {
            return console.log(error);
        }
        else {
            console.log("File Read Successfully")
            let uppercaseContent = data.toUpperCase();
            fs.writeFile(path.join(__dirname, "uppercaseLipsum.txt"), uppercaseContent, (error) => {
                if (error) {
                    return console.log(error);
                }
                else {
                    console.log("File content converted to Uppercase")
                    fs.writeFile(path.join(__dirname, "filenames.txt"), "uppercaseLipsum.txt", (error) => {
                        if (error) {
                            return console.log(error);
                        }
                        else {
                            console.log("Uppercase content file name is added into filenames.txt")
                            fs.readFile(path.join(__dirname, "uppercaseLipsum.txt"), "utf8", (error, data) => {
                                if (error) {
                                    return console.log(error);
                                }
                                else {
                                    let lowercaseContent = data.toLocaleLowerCase();
                                    let splittedData = lowercaseContent.split(" .");
                                    fs.writeFile(path.join(__dirname, "lowerAndSplit.txt"), JSON.stringify(splittedData), (error) => {
                                        if (error) {
                                            return console.log(error);
                                        }
                                        else {
                                            console.log("File converted to lowercase and splitted into sentences")
                                            fs.appendFile(path.join(__dirname, "filenames.txt"), "\nlowerAndSplit.txt", (error) => {
                                                if (error) {
                                                    return console.log(error);
                                                }
                                                else {
                                                    console.log("Lower case content file name is added into filename.txt")
                                                    fs.readFile(path.join(__dirname, "lowerAndSplit.txt"), "utf8", (error, data) => {
                                                        if (error) {
                                                            return console.log(error);
                                                        }
                                                        else {
                                                            let sortedData = JSON.parse(data).sort((firstSentence, secondSentence) => {
                                                                if (firstSentence > secondSentence) {
                                                                    return 1;
                                                                }
                                                                else if (firstSentence < secondSentence) {
                                                                    return -1;
                                                                }
                                                                else {
                                                                    return 0;
                                                                }
                                                            })

                                                            fs.writeFile(path.join(__dirname, "sortedLipsum.txt"), JSON.stringify(sortedData), (error) => {
                                                                if (error) {
                                                                    return console.log(error);
                                                                }
                                                                else {
                                                                    console.log("Sentences of file have been sorted alphabetically")
                                                                    fs.appendFile(path.join(__dirname, "filenames.txt"), "\nsortedLipsum.txt", (error) => {
                                                                        if (error) {
                                                                            return console.log(error);
                                                                        }
                                                                        else {
                                                                            console.log("Sorted File name is added to filenames.txt")
                                                                            fs.readFile(path.join(__dirname, "filenames.txt"), "utf8", (error, data) => {
                                                                                if (error) {
                                                                                    return console.log(error);
                                                                                }
                                                                                else {
                                                                                    let fileArray = data.split("\n");
                                                                                    fileArray.map((file) => {
                                                                                        fs.unlink(path.join(__dirname, file), (error) => {
                                                                                            if (error) {
                                                                                                return console.log(error);
                                                                                            }
                                                                                            else {
                                                                                                console.log(`${file} removed successfully`);
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}


module.exports = performMultipleActions