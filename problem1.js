const fs = require('fs');
const path = require('path');

const createFileAndDelete = () => {
    fs.mkdir(path.join(__dirname, "dir1"), { recursive : true }, (error) => {
        if(error) {
            return console.log(error);
        }
        else {
            console.log('Directory created successfully')
            let fileNames = ['file1.js', 'file2.js', 'file3.js', 'file4.js', 'file5.js'];
            fileNames.map((file) => {
                fs.writeFile(path.join(__dirname, "dir1", file), "Hello", (error) => {
                    if(error) {
                        return console.log(error);
                    } else {
                        console.log(`${file} is created`);
                        fs.unlink(path.join(__dirname, "dir1", file), (error) => {
                            if(error) {
                                return console.log(error);
                            } else {
                                console.log(`${file} removed successfully`)
                            }
                        })
                    }
                })
            })
        }
    })
}

module.exports = createFileAndDelete;